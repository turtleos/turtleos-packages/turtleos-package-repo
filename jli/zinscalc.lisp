;;; jlidemo --- Summary
;;; Commentary:
;;; Code:
(print "+++\nZinsenrechner\n+++")
(setq *start* (parse-integer (read "Betrag: ")))
(setq *zinssatz* (/ (read "Zinssatz: ") 100))
(setq *years* (read "Jahre: "))
(setq *zinsen* 0)
(setq *ende* (*start*))

(print (*start*))

(loop for y from 1 to (*years*) do
(print "############")
(print (concat "# Jahr:   " (y)))
(print (concat "# Anfang: " (*start*)))
(setq *zinsen* (* (*start*) (*zinssatz*)))
(print (concat "# Zinsen: " (*zinsen*)))
(setq *ende* (+ (*start*) (*zinsen*)))
(print (concat "# Ende:   " (*ende*)))
(setq *start* (*ende*)))
